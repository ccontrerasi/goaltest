//
//  BeerListInteractor.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation
import Moya

class BeerListInteractor : BeerListInteractorInterface {
    
    private static var sharedBeerListInteractor: BeerListInteractor = {
        let sharedBeerListInteractor = BeerListInteractor()
        
        return sharedBeerListInteractor
    }()

    private init() {
        
    }

    class func shared() -> BeerListInteractorInterface {
        return sharedBeerListInteractor
    }
    
    let provider = MoyaProvider<MyService>()
    var output : BeerListInteractorOutput?
    
    /*
     Method that obtain all beers with some filters
     */
    func getBeers(page: Int, name: String = "") {
        
        self.provider.request(.getBeers(page: page, perPage: PAGINATION_SIZE, beer_name: name)) { result in
            //print("Result: \(result)")
            
            switch result {
            case let .success(moyaResponse):
                do {
                    let data = try moyaResponse.mapJSON()
                    print("Response: \(data)")
                    
                    let decoder = JSONDecoder()
                    //decoder.dateDecodingStrategy = .formatted(.dateFromServerFull)
                    
                    let apiResponse = try decoder.decode(Array<Beer>.self, from: moyaResponse.data)
                    
                    let getBeersResponse = GetBeersResponse()
                    getBeersResponse.beers = apiResponse
                    getBeersResponse.page = page
                    
                    self.output?.processAction(.getBeersLoaded, params: getBeersResponse)
                    
                }  catch {
                    // Error in invokation
                    print("Error: \(error)")
                    self.output?.processError(.getBeersLoadedError, code: nil)
                }
                
            // do something in your app
            case let .failure(error):
                print("Error: \(error)")
                self.output?.processError(.getBeersLoadedError, code: nil)
                break
            }
        }
    }
    
    
    
}
