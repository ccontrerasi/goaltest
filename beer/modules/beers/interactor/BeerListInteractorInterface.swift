//
//  BeerListInteractorInterface.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

protocol BeerListInteractorInterface {
    
    var output : BeerListInteractorOutput? {get set}
    
    func getBeers(page : Int, name: String)
}
