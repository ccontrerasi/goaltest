//
//  BeerListViewController.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AlamofireImage

class BeerListViewController: UIBaseViewController {
    
    var presenter : BeerListPresenterInterface?
    @IBOutlet weak var pnlTop: UIView!
    @IBOutlet weak var pnlSearch: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    var currentPage = 1
    
    @IBOutlet weak var txtSearch: UITextField!
    
    var task : DispatchWorkItem?
    var query = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pnlTop.backgroundColor = ORANGE_COLOR
        pnlSearch.layer.cornerRadius = 10
        
        txtSearch.rx.text.orEmpty.distinctUntilChanged().bind { query in
            self.loadFiltering(query: query)
        }.disposed(by: disposeBag)
        
        tableView.register(UINib(nibName: "BeerResultTableViewCell", bundle: nil), forCellReuseIdentifier: "BeerResultTableViewCell")
        
        presenter?.beers.bind(to: tableView.rx.items) { (tableView, row, beer) in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BeerResultTableViewCell") as! BeerResultTableViewCell
            
            cell.imgBeer.af.cancelImageRequest()
            
            if (beer.image_url != nil){
                if let url = URL(string: beer.image_url!) {
                    cell.imgBeer.af.setImage(withURL: url)
                }
            }
            
            cell.lblTitle.text = beer.name
            cell.lblDescription.text = beer.description
            
            return cell
            
        }.disposed(by: disposeBag)
        
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Beer.self).subscribe(onNext: { beer in
            
            self.presenter?.goToDeatail(beer: beer, nav: self.navigationController)
            
        }, onError: { error in
            print("Error: \(error)")
        }).disposed(by: disposeBag)
        
        showProgress()
        presenter?.getBeers(page: currentPage, name: query)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func loadFiltering(query : String) {
        if (task?.isCancelled == false){
            task?.cancel()
        }
        
        task = DispatchWorkItem {
            self.showProgress()
            
            self.query = query
            self.presenter?.clearBears()
            self.currentPage = 1
            self.presenter?.getBeers(page: self.currentPage, name: query)
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: task!)
        
    }
}

extension BeerListViewController : BeerListViewInterface {
    func postAction(_ action: ActionsViewListBeers, params: Any?) {
        hideProggres()
        
        switch action {
        case .beers_loaded:
            if let result = params as? GetBeersResponse {
                
                if (!result.beers.isEmpty){
                    self.currentPage = result.page
                }
            }
            break
        }
    }
    
    func showError(_ error: ErrorsViewListBeers) {
        print("Error")
    }
}

extension BeerListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.row == ((self.currentPage * PAGINATION_SIZE) - 1)){
            // We are at last row
            
            showProgress()
            presenter?.getBeers(page: currentPage + 1, name: query)
        }
    }
}
