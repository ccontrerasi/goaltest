//
//  BeerDetailViewController.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import UIKit

class BeerDetailViewController: UIBaseViewController {
    
    @IBOutlet weak var pnlTop: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBeer: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var presenter : BeerListPresenterInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pnlTop.backgroundColor = ORANGE_COLOR
        
        let beer = presenter?.beerDetail
        
        lblTitle.text = beer?.name
        lblName.text = beer?.name
        lblDescription.text = beer?.description
        
        if (beer?.image_url != nil){
            if let url = URL(string: beer!.image_url!) {
                imgBeer.af.setImage(withURL: url)
            }
        }
        
    }
    
    
    @IBAction func accBack(_ sender: Any) {
        presenter?.goBack(controller: self)
    }
    
    
}


extension BeerDetailViewController : BeerListViewInterface {
    func postAction(_ action: ActionsViewListBeers, params: Any?) {
        hideProggres()
    }
    
    func showError(_ error: ErrorsViewListBeers) {
        print("Error")
    }
}
