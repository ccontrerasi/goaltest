//
//  BeersRouting.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation
import UIKit

class BeersRouting: BeersRoutingInterface {
    
    private static var sharedBeersRouting: BeersRouting = {
        let sharedBeersRouting = BeersRouting()
        
        return sharedBeersRouting
    }()

    private var presenter: BeerListPresenter
    private var interactor: BeerListInteractorInterface

    private init() {
        interactor = BeerListInteractor.shared()
        presenter = BeerListPresenter()
        presenter.interactor = interactor
        interactor.output = presenter
        presenter.routing = self
    }

    class func shared() -> BeersRoutingInterface {
        return sharedBeersRouting
    }

    
    func initializeModule() -> BeerListViewController {
        
        let vc = UIStoryboard(name:"Beers",bundle: Bundle(for: BeerListViewController.self)).instantiateViewController(identifier: "BeerListViewController") as! BeerListViewController
        
        presenter.view = vc
        vc.presenter = presenter
        
        return vc
    }
    
    func goToDetail(beer : Beer, nav : UINavigationController?){
        
        let vc = UIStoryboard(name:"Beers",bundle: Bundle(for: BeerListViewController.self)).instantiateViewController(identifier: "BeerDetailViewController") as! BeerDetailViewController
        
        presenter.beerDetail = beer
        presenter.view = vc
        vc.presenter = presenter
        
        nav?.pushViewController(vc, animated: true)
    }
    
    func goBackNav(controller : UIViewController){
        
        controller.navigationController?.popViewController(animated: true)
        
        if let cnt = controller.navigationController?.visibleViewController as? BeerListViewInterface {
            presenter.view = cnt
        }
    }
}
