//
//  BeerListPresenterInterface.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation
import RxCocoa

protocol BeerListPresenterInterface {
    
    var interactor: BeerListInteractorInterface? {get}
    var beers : BehaviorRelay<[Beer]> {get}
    
    var beerDetail : Beer? {get}
    
    func getBeers(page : Int, name: String)
    func clearBears()
    
    func goToDeatail(beer: Beer, nav : UINavigationController?)
    func goBack(controller : UIViewController)
}
