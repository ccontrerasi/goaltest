//
//  BeerListPresenter.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BeerListPresenter: BeerListPresenterInterface {
    
    var beerDetail: Beer?
    var interactor: BeerListInteractorInterface?
    var routing: BeersRoutingInterface?
    var view: BeerListViewInterface?
    
    var beers = BehaviorRelay<[Beer]>(value: [])
    
    func getBeers(page : Int, name: String) {
        interactor?.getBeers(page: page, name: name)
    }
    
    func clearBears() {
        beers.accept([])
    }
    
    func goToDeatail(beer: Beer, nav : UINavigationController?) {
        routing?.goToDetail(beer: beer, nav: nav)
    }
    
    func goBack(controller : UIViewController){
        routing?.goBackNav(controller: controller)
    }
}


extension BeerListPresenter : BeerListInteractorOutput {
    func processAction(_ type: BeerListOutputActions, params: Any?) {
        switch type {
        case .getBeersLoaded:
            view?.postAction(.beers_loaded, params: params)
            
            // We're going to inyect into berrs result
            if let result = params as? GetBeersResponse {
                
                if (!result.beers.isEmpty){
                    beers.accept(beers.value + result.beers)
                }
            }
        }
    }
    
    func processError(_ type: BeerListOutputErrors, code: Any?) {
        view?.showError(.generic)
    }
    
    
}
