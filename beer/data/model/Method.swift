//
//  Method.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

class Method: Codable {
    var mash_temp : [MashTemp]? = []
    var fermertation : Fermetation?
    var twist : String?
}
