//
//  Hop.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

class Hop: Codable {
    let name: String?
    let amount: Unity?
    let add, attribute: String?
}
