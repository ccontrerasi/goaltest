//
//  MashTemp.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

class MashTemp: Codable {
    var temp : Unity?
    var duration : Int? = 0
}
