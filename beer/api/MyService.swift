//
//  MyService.swift
//  beer
//
//  Created by Cristian Contreras on 13/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

import Foundation
import Moya

enum MyService {
    case getBeers(page: Int, perPage: Int, beer_name: String)
    
}

extension MyService : TargetType {
    
    var baseURL: URL { return URL(string: BASE_URL)! }
    
    var path: String {
        switch self {
        case .getBeers(_, _, _):
            return "beers"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getBeers( _, _, _):
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getBeers(let page, let perPage, let beerName):
            return "Called to get-beers with page: \(page) and perPage: \(perPage) and beername: \(beerName).".utf8Encoded
        }
        
    }
    
    var task: Task {
        switch self {
        case .getBeers(let page, let perPage, let beerName):
            
            var params : [String : Any] = [:]
            
            params["page"] = page
            params["per_page"] = perPage
            
            if (!beerName.isEmpty){
                params["beer_name"] = beerName
            }
            
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
        
    }
    
    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
}
