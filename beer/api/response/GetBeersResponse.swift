//
//  GetBeersResponse.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation

class GetBeersResponse {
    
    var beers : [Beer] = []
    var page = 1
    
}
