//
//  UIBaseViewController.swift
//  beer
//
//  Created by Cristian Contreras on 14/05/2020.
//  Copyright © 2020 Cristian Contreras. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD

public class UIBaseViewController: UIViewController {
    let hud = JGProgressHUD(style: .extraLight)
    var controlKeyboard = false
    
    public override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= keyboardFrame.height
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y += keyboardFrame.height
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (controlKeyboard){
            NotificationCenter.default.addObserver(self, selector: #selector(UIBaseViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(UIBaseViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (controlKeyboard){
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        }
        
        self.view.endEditing(true)
    }
    
    func showMessage(_ message : String? = nil, indicator : JGProgressHUDImageIndicatorView = JGProgressHUDSuccessIndicatorView()){
        let hud = JGProgressHUD(style: .extraLight)
        hud.textLabel.text = message
        hud.indicatorView = indicator
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 1.0)
    }
    
    func showMessageAlert(message : String){
        let alert = UIAlertController(title: getString("aviso"), message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: getString("ok"), style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showProgress(){
        hud.show(in: self.view, animated: true)
    }
    
    func hideProggres() {
        hud.dismiss(animated: true)
    }
    
    func getBundle() -> Bundle{
        return Bundle(for: self.classForCoder)
    }
    
    func getString(_ key : String) -> String{
        return NSLocalizedString(key, tableName: nil, bundle: getBundle(), value: key, comment: "")
    }
    
    func getString(_ key : String, parameters : [String]) -> String{
        let str = NSLocalizedString(key, tableName: nil, bundle: getBundle(), value: key, comment: "")
        
        return String(format: str, arguments: parameters)
    }

}
